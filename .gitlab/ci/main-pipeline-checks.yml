
# ==========================================
# Test stage
# ==========================================

.generator-base:
  stage: test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - charts/**/*
        - kustomize-units/**/*
        - environment-values/**/*
        - .gitlab/ci/scripts/generate-pipeline.sh
        - .gitlab/ci/configuration/*
        - .gitlab/ci/main-pipeline-checks.yml
        - .gitlab/ci/chart-jobs.yml
        - tools/generate_json_schema.py

chart-generator:
  extends: .generator-base
  script:
    - echo "Generate CI jobs for each modified charts/units/environment-values"
    - ./.gitlab/ci/scripts/generate-pipeline.sh > generated-pipeline.yml
  artifacts:
    expire_in: 1 hour
    paths:
      - generated-pipeline.yml

chart-jobs:
  extends: .generator-base
  needs:
    - chart-generator
  trigger:
    include:
      - artifact: generated-pipeline.yml
        job: chart-generator
    strategy: depend


.lint-base:
  stage: test
  needs: []
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: '$CI_PIPELINE_SOURCE == "pipeline"'

yamllint:
  extends: .lint-base
  script:
    - 'yamllint . -d "$(cat < .gitlab/ci/configuration/yamllint.yaml) $(cat < .gitlab/ci/configuration/yamllint-helm-exclude-charts.yaml)"'

avoid-typo-on-bootstrap:
  extends: .lint-base
  script:
    - |
      rm -rf .git  # because busybox grep does not support --exclude-dir
      echo "Check against frequent typos on 'bootstrap'..."
      set +e
      typos=$(grep -rnsiE 'boostrap|bootrap|bootsrap' . | grep -v '.gitlab-ci.yaml:      typos=')
      set -e
      if [ -n "$typos" ]; then
        echo "A few typos were found on the 'bootstrap' word:"
        echo "-----------------"
        echo "$typos"
        echo "-----------------"
        exit 1
      fi

check-docs-markdown:
  extends: .lint-base
  image: registry.gitlab.com/gitlab-org/gitlab-docs/lint-markdown:alpine-3.16-vale-2.20.2-markdownlint-0.32.2-markdownlint2-0.5.1
  script:
    - git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    - |
      md_files=$(git diff --name-only $CI_COMMIT_SHA origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME | grep "\.md$" || true)
      if [ -n "$md_files" ] ; then
        markdownlint-cli2-config .gitlab/ci/configuration/.markdownlint.yml $md_files
      else
        echo "No modified .md files"
      fi
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      changes:
        - "**/*.md"


check-sylva-toolbox-binaries-versions:
  extends: [.lint-base, .docker-service]
  script:
    - eval $(grep ^SYLVA_TOOLBOX_ tools/shell-lib/common.sh)
    - mkdir bin
    - docker run --rm ${SYLVA_TOOLBOX_REGISTRY}/${SYLVA_TOOLBOX_IMAGE}:${SYLVA_TOOLBOX_VERSION} | tar xz -C bin
  # this job checks that the different versions of binaries that we use are aligned:
  # - flux binary in sylva-toolbox
  # - flux kustomization (kustomize-units/flux-system)
  # - capi kustomization (kustomize-units/capi)
  # - clusterctl version in sylva-toolbox
    - flux_binary_version=$(bin/flux version --client -o yaml | yq .flux)
    - kustomization_version=$(yq '.resources[0] | capture("/(?P<version>v[^/]+)/") | .version' kustomize-units/flux-system/base/kustomization.yaml)
    - clusterctlbinary_version=$(bin/clusterctl version  -o yaml | yq .clusterctl.gitVersion)
    - capi_version_check=$(yq '.resources[0] | capture("/(?P<version>v[^/]+)/") | .version' kustomize-units/capi/kustomization.yaml)
    - |
      if [[ $flux_binary_version != $kustomization_version ]]; then
        echo
        echo "Flux version mismatch between the 'flux' binary provided in sylva-toolbox and the Flux version used in kustomize-units/flux-system"
        echo "- flux binary has version $flux_binary_version"
        echo "- flux kustomization has  $kustomization_version"
        echo
        exit_code=1
      fi
    - |
      if [[ $clusterctlbinary_version != $capi_version_check ]]; then
        echo
        echo "Clusterctl version mismatch between the 'clusterctl' binary provided in sylva-toolbox and the 'capi' version used in kustomize-units/capi"
        echo "- capi kustomization has version $capi_version_check"
        echo "- clusterctl version in sylva-toolbox has version  $clusterctlbinary_version"
        echo
        exit_code=1
      fi
    - exit ${exit_code:-0}

clusterctl-check-versions:
   # this job checks that the versions of clusterctl and capi that we use are aligned:
   # - capi kustomization (kustomize-units/capi)
   # - clusterctl version in kube-job image
  extends: [.lint-base, .docker-service]
  script:
    - capi_version=$(yq '.resources[0] | capture("/(?P<version>v[^/]+)/") | .version' kustomize-units/capi/kustomization.yaml)
    - clusterctl_kubejob_version=$(docker run --rm $(yq eval 'select(documentIndex==1).spec.template.spec.containers[0].image' kustomize-units/kube-job/job.yaml) sh -c 'clusterctl version | grep -o "GitVersion:\"[^\"]*" | cut -d"\"" -f2')
    - |
      if [[ $capi_version != $clusterctl_kubejob_version ]]; then
        echo
        echo "Version mismatch between the 'capi' version used in kustomize-units/capi and the 'clusterctl' version used in kube-job image"
        echo "- capi kustomization has version $capi_version"
        echo "- clustertctl version in kube-job image has version  $clusterctl_kubejob_version"
        exit -1
        echo
      fi
